	=============================================================
	#############################################################
	-------------------------------------------------------------
	NinjaBlockMan

	Java game developed by Craig Childs, 311143
	Northbrook 2012 -- Sue Messingham -- Unit 22 Game Development
	-------------------------------------------------------------
	#############################################################

	Version log:
	-------------------------------------------------------------
	0.0:
	 -Movement
	 -Inventory
	 -Level system
	 -Platforms
	 -Score
	 
	0.1:
	 -Improved graphics
	 -Facing flags
	 -Improved game/class structure
	 -In game events.
	 -Starting menu.
	 -Projectiles
	 
	0.2:
	 -Improved jumping
	 -Improved tiling
	 -Yet more graphics improvements
 	 -Rewrite collision detection for different platform types.

	Development Log:
	-------------------------------------------------------------

	Current Task(s):
	 -Animation
	 -Finish Projectiles
	 -Implement a danger/objective/opponent
	 -Implement more platform/block types


	To Do:
	 -Design multiple levels
	 -Add more menu options
	 -Add options
	 -Add controls/instructions
	 -Add high score list
	 -Add melee attack
	 -AI?
	 -Multiplayer?
	 -Add sounds
	 -Add cut-scene
	 
	-------------------------------------------------------------
	=============================================================
