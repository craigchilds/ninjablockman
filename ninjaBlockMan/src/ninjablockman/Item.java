package ninjablockman;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Item {
    Boundary bound = new Boundary();
    Rectangle rect = new Rectangle();
    Rectangle[] RectangleArray;
    String type;
    Image img;
    int x, y, width, height, ammo = 50;
    boolean isHit = false;
    boolean canShoot = true;

    Item(String type, int x, int y) {
        this.type = type;
        this.x = x;
        this.y = y;
        
    }
    
    public void draw(Graphics g){
        if(!isHit){
            if(this.type.equals("KeyCard")) {
                this.width = 5;
                this.height = 10;
                g.setColor(new Color(78, 138, 70));
                g.fillRect(this.x, this.y, this.width, this.height);
                genRect();
            }else if(this.type.equals("Med Pack")){
                this.width = 10;
                this.height = 10;
                setImage("images/medpack.png");
                g.drawImage(img, this.x, this.y, null);
                //g.setColor(new Color(195, 74, 50));
                //g.fillRect(this.x, this.y, this.width, this.height);
                genRect();
            }else if(this.type.equals("Gun")){
                this.width = 10;
                this.height = 5;
                g.setColor(new Color(109, 109, 109));
                g.fillRect(this.x, this.y, this.width, this.height);
                g.fillRect(this.x + this.height, this.y, this.height, this.width);
                genRect();
            }
            
        }
    }
    
    public void setImage(String value){
       try {
           this.img = ImageIO.read(new File(value));
       }catch(IOException e){
           System.out.println(e);
       }
   }
    
    private void genRect(){
        rect.x = this.x;
        rect.y = this.y;
        rect.width = this.width;
        rect.height = this.height;
        
        //Create the rectangle array
        RectangleArray = new Rectangle[1];
        RectangleArray[0] = rect;
    }
    
    //Has the player picked up the item
    public void playerPickup(Player player){
        if(!isHit){
            bound.checkCollision(player.Rect, RectangleArray);
            if(bound.collision[0]){
                isHit = true;
                player.addToInv(this);
                if(this.type.equals("Gun")){
                    this.ammo += 10;
                }
                
                if(this.type.equals("KeyCard")){
                    player.holdingKey = true;
                }
            }
        }
    }
    
    public void doFunction(String item, Player player){
        if(item.equals("Gun")){
            shoot(player);
        }else if(item.equals("Med Pack")){
            addHealth(player);
        }
    }
    
    public void update(Graphics g, Player player){
        playerPickup(player);
    }
       
    private void shoot(Player player){
        if(this.ammo > 0){
            this.ammo -= 1;
            System.out.println("You shot your weapon.");
            player.shoot();
        }else{
            System.out.println("You have no ammo.");
            this.canShoot = false;
        }
    }
    
    private void addHealth(Player player){
        player.health += 35;
        System.out.println(player.health);
    }
}
