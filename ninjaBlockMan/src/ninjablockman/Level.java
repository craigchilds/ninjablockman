package ninjablockman;

import java.awt.Graphics;
import java.util.concurrent.CopyOnWriteArrayList;

public class Level {
    int level = 0, maxPlatforms = 10, maxLadders = 10;
    Player player;
    Enemy turret = new Enemy(
            //Health
            100,
            //Damage
            10,
            //Score
            0,
            //Speed
            0f,
            //xPos
            400,
            //yPos
            30,
            //height
            30,
            //width
            30,
            //type
            "Turret");
    /**
     * LEVEL 1 DEFINITIONS
     */
    Platform ladder = new Platform();
    Platform ladder2 = new Platform();
    Platform platform2 = new Platform();
    Platform platform3 = new Platform();
    Platform platform4 = new Platform();
    Platform platform5 = new Platform();
    Platform ladder3 = new Platform();
    Platform wallRight = new Platform();
    Platform wallLeft = new Platform();
    Platform trap1 = new Platform();
    Platform[] platformArray;
    Platform[] ladderArray;
      
    Object score1 = new Object(7, 9, 20, 20,"Score");
    Object score2 = new Object(3, 5, 20, 20,"Score");
    Object score3 = new Object(4, 3, 20, 20,"Score");
    Object score4 = new Object(6, 8, 20, 20,"Score");
    Object door = new Object(7, 7, 10, 30,"Door");
    
    Item key = new Item("KeyCard", 255, 45);
    Item medpack = new Item("Med Pack", 200, 45);
    Item gun = new Item("Gun", 280, 45);
    
    /**
     * END OF LEVEL 1 DEFINITIONS
     */
    
    public Level(Player player, int maxPlatforms){
        this.player = player;
        this.maxPlatforms = maxPlatforms;
        this.maxLadders = maxPlatforms;
    }
    
      public final void level1(){
        
        this.level = 1;  
        //Start bringing out platforms into play.
        ladder.newPlatform(5, 8, 1 ,6, "images/ladder2.png", "ladder");
        ladder2.newPlatform(11, 9, 1 ,2, "images/ladder2.png", "ladder");
        platform2.newPlatform(0, 8, 6 ,1, "images/brick2.png", "platform");
        platform3.newPlatform(5, 11, 7 ,1, "images/brick2.png", "platform");
        platform4.newPlatform(11, 9, 9, 1, "images/brick2.png", "platform");
        platform5.newPlatform(12,5,7,1,"images/brick2.png", "platform");
        ladder3.newPlatform(14,5,1,4,"images/ladder2.png", "ladder");

        wallLeft.newPlatform(0, 0, 1, 17, "images/brick2.png", "platform");
        wallRight.newPlatform(19, 0, 1, 17, "images/brick2.png", "platform");

        //Set out traps
        
        //add them to an array
        platformArray = new Platform[4];
        ladderArray = new Platform[3];
        
        ladderArray[0] = ladder;
        ladderArray[1] = ladder2;
        ladderArray[2] = ladder3;
        
        platformArray[0] = platform2;
        platformArray[1] = platform3;
        platformArray[2] = platform4;
        platformArray[3] = platform5;

        //pass them to the player so that he knows that they are there, e.g. collision detection
        player.addPlatformAwareness(platformArray, ladderArray); 
    }
      
      public final void level2(){
          this.level = 2;
          player.setXPos(100);
          platformArray = new Platform[4];
          ladderArray = new Platform[3];
          player.addPlatformAwareness(platformArray, ladderArray);          
      }
    
    public void draw(Graphics g){
            if(this.level == 1){
                //Here we draw the first level
                
                turret.draw(g);
                platform2.draw(g);            
                platform3.draw(g);
                platform4.draw(g);
                platform5.draw(g);
                ladder3.draw(g);
                wallRight.draw(g);
                wallLeft.draw(g);
                ladder.draw(g);

                ladder2.draw(g);
                
                player.draw(g);
                //trap1.draw(g);

                //SCORE
                score1.draw(g);
                score2.draw(g);
                score3.draw(g);
                score4.draw(g);

                //ITEMS
                key.draw(g);
                key.update(g, player);
                medpack.draw(g);
                medpack.update(g, player);
                gun.draw(g);
                gun.update(g, player);

                //Doors
                door.draw(g);
                door.update(g, player);

                //SCORE
                score1.update(g, player);
                score2.update(g, player);
                score3.update(g, player);
                score4.update(g, player);
            }
            
            if(this.level == 2){
                player.draw(g);
            }
    }
    
}
