package ninjablockman;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;

public class Platform {
    Rectangle Rect = new Rectangle();
    Rectangle Top = new Rectangle();
    Boundary bound = new Boundary();
    Graphics g;
    String type;
    int damage = 10;
    
    int x, y, height, width;
    Image image;
    boolean dangerous = false;
    public boolean isDrawn = false;

    //Create a new platform from the foreground class.
    public void newPlatform(int xPos, int yPos, int Width, int Height, String image, String type){

        
        this.x = xPos * 30;
        this.y = (yPos * 30) - 20;
        this.height = Height * 30;
        this.width = Width * 30;
        this.type = type;

        Foreground fore = new Foreground("Platform", this.x, this.y,  this.width, this.height);

        this.Rect = fore.getPlatform();
        this.Top = fore.getTop();

        setImage(image);
    }

    //Draw the platform
    public void draw(Graphics g){
        g.setColor(new Color(155, 115, 44));
        //g.fillRect(this.x, this.y, this.width, this.height);
        g.drawImage(this.image, x, y, null);
        
       for(int i = this.x; i < this.x + this.width; i += 30){
           g.drawImage(this.image, i, this.y, null);
       }
       
       for(int i = this.y; i< this.y + this.height; i+= 30){
           g.drawImage(this.image, this.x, i, null);
       }

       this.isDrawn = true;
    }

    private void setImage(String value){
       // Gonna need this when doing the final build
       //  URL url = getClass().getResource(value);
       
       try {
           this.image = ImageIO.read(new File(value));
       }catch(IOException e){
           System.out.println(e);
       }
       
       
       //System.out.println(url.toString());
    }

    //Is this platform dangerous?
    public void setDanger(boolean value){
        this.dangerous = value;
    }
    
}