package ninjablockman;

import java.awt.Rectangle;

public class Foreground {
    Rectangle Rect = new Rectangle();
    Rectangle Top = new Rectangle();
    
    Foreground(String type, int xPos, int yPos, int Width, int Height) {
        //Set the rectangle
        if(type.equals("Platform")){
            this.Rect.height = Height;
            this.Rect.width = Width;
            this.Rect.x = xPos;
            this.Rect.y = yPos;
            
            this.Top.y = yPos;
            this.Top.x = xPos;
            this.Top.height = 1;
            this.Top.width = Width;
        }
        
    }

    //Return the rectangle for the platform.
    public Rectangle getPlatform(){
        return this.Rect;
    }
    
    public Rectangle getTop(){
        return this.Top;
    }
}
