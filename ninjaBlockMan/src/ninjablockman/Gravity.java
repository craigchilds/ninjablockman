/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ninjablockman;

import java.awt.Rectangle;

public class Gravity {
    public int gravity = 2;
    public int floor = 367;
    public boolean onFloor = false;
    public boolean onPlatform = false, onLadder = true;

    Rectangle[] Platforms, Ladders;
    
    Boundary bound = new Boundary();

    Gravity() {
        //The constructor
    }
 
    //apply gravity
    public void apply(Player player){
        gravity = 2;
        //If the player is above the floor, make them fall
        if(player.yPos < floor){
            player.yPos += gravity;
        } 
        
    }
    
    //remove gravity
    public void remove(Player player){
       gravity = 0;
    }

    //Check if the player is on the flood
    public void onFloor(Player player){
        if(player.yPos == this.floor){
            onFloor = true;
        }else{
            onFloor = false;
        }
    }

    //extract the rectangles from the platform array.
    public void getPlatformRectangles(Platform[] Platform){
       Platforms = new Rectangle[Platform.length];

       for(int i = 0; i < Platform.length; i++){
            Platforms[i] = Platform[i].Top;
            //System.out.println(PlatformRect[i]);
       }
    }
    
    //extract the rectangles from the platform array.
    public void getLadderRectangles(Platform[] Platform){
       Ladders = new Rectangle[Platform.length];

       for(int i = 0; i < Platform.length; i++){
            Ladders[i] = Platform[i].Rect;
            //System.out.println(PlatformRect[i]);
       }
    }
    
    public void platformCheck(Player player,  Platform[] platform){
      
      getPlatformRectangles(platform);
      
      //run the check collision method. This will set bound.collision array
      bound.checkCollision(player.feet, this.Platforms);
      
      //loop through the platforms to see if  a collision has occurred.
      for(int i = 0; i < bound.collision.length; i++){
          onPlatform = bound.collision[i];
          //if we are on a platform we want to break out of the loop.
          if(onPlatform){
              break;
          }          
      }
      
      //System.out.println(player.Rect.y + player.Rect.height);
    }
    
     public void ladderCheck(Player player,  Platform[] ladders){
      
      getLadderRectangles(ladders);
      
      //run the check collision method. This will set bound.collision array
      bound.checkCollision(player.Rect, this.Ladders);
      
      //loop through the platforms to see if  a collision has occurred.
      for(int i = 0; i < bound.collision.length; i++){
          onLadder = bound.collision[i];
          //if we are on a platform we want to break out of the loop.
          if(onLadder){
              break;
          }          
      }
      
      //System.out.println(player.Rect.y + player.Rect.height);
    }
    
    //return the value of onFloor
    public boolean getOnFloor(){
        return onFloor;
    }

    //Set floor value
    public void setFloor(int value){
        floor = value;
    }

}