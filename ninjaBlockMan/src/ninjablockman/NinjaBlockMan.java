package ninjablockman;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.RepaintManager;
import javax.swing.Timer;


public class NinjaBlockMan extends JFrame implements KeyListener{
    
    Player player = new Player(
            //Health
            100,
            //Damage
            10,
            //Score
            0,
            //Speed
            1.0f,
            //xPos
            50,
            //yPos
            8,
            //height
            31,
            //width
            10);
    
    
    private Graphics Graphics;
    private Image image;
    Gravity gravity = new Gravity();
    Level level =  new Level(player, 7);
    Background bg = new Background();
    
    public long delay = 300;
    
    public int currentLevel = 1;
    

    RepaintManager repaintManager = new RepaintManager();
        
      
    public NinjaBlockMan(){
        //Create our menu window
        initializeWindow();
        gravity.setFloor(400);
                        
        //Start a timer process so that we can then time the player.               
        //Process timer = new Process();
        //timer.Start();

    }
    
    /**
     * @desc set up our window for us.
     */
    private void initializeWindow(){
        setSize(600, 400);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addKeyListener(this);
        setLocationRelativeTo(null);
        setTitle("Ninja Block Man");
        setVisible(true);
        setBackground(new Color(105, 161, 208));
    }
    
    // I WANT THIS DONE WHICH WILL INITIALIZE THE PROGRAM
       
    @Override
    public void paint(Graphics g){
       repaintManager.setDoubleBufferingEnabled(true);
       image = createImage(getWidth(), getHeight());
       Graphics = image.getGraphics();
       paintComponent(Graphics);
       g.drawImage(image, 0, 0, null);
       
       
       
       //This should only run repaint() at 50fps.
       //http://stackoverflow.com/questions/2972651/java-repaint-component-every-second
       ActionListener taskPerformer = new ActionListener() {
          //@Override
          public void actionPerformed(ActionEvent evt) {
           repaint();
          }
       };

       new Timer((int) delay,taskPerformer).start();
       
       isLevelComplete(player);
    }
        
    public void paintComponent(Graphics g){
       if(currentLevel == 1){
            level.level1();
       }else if(currentLevel == 2){
            level.level2();
       }
        
        //Draw our components to the floor.
        bg.draw(g);
        drawLevel(g);
        drawUI(g);
    }
     
   
    @Override
    public void keyReleased(KeyEvent key){
        if(key.getKeyCode() == KeyEvent.VK_LEFT){
            // Player moves left
            player.setLeft(false);
        }else if(key.getKeyCode() == KeyEvent.VK_RIGHT){
            // Player moves right
            player.setRight(false);
        }else if(key.getKeyCode() == KeyEvent.VK_UP){
            // Player moves right
            player.setJump(false);
            player.setHasJumped(false);
        }else if(key.getKeyCode() == KeyEvent.VK_DOWN){
            // Player moves right
            player.setDuck(false);
        }

        if(key.getKeyCode() == KeyEvent.VK_S){
            player.sprintStop();
        }
       
    }
    
    @Override
    public void keyPressed(KeyEvent key){
        if(key.getKeyCode() == KeyEvent.VK_LEFT){
            // Player moves left
            //player.setLeft(true);
            player.walking_left = true;
        }else if(key.getKeyCode() == KeyEvent.VK_RIGHT){
            // Player moves right
            player.walking_right = true;
        }else if(key.getKeyCode() == KeyEvent.VK_UP){
            // Player moves right
            player.jumping = true;
        }else if(key.getKeyCode() == KeyEvent.VK_DOWN){
            // Player moves right
            player.ducking = true;
        }else if(key.getKeyCode() == KeyEvent.VK_Q){
            player.use(0);
        }else if(key.getKeyCode() == KeyEvent.VK_W){
            player.use(1);
        }else if(key.getKeyCode() == KeyEvent.VK_E){
            player.use(2);
        }else if(key.getKeyCode() == KeyEvent.VK_R){
            player.use(3);
        }

        // This is our sprint
        if(key.getKeyCode() == KeyEvent.VK_S){
            player.sprint();
        }
    }
    
    @Override
    public void keyTyped(KeyEvent key){
        //Don't need to do anything here.
    }
    
    /**
     * @DESC Create the level, and all of its components.
     */
  
    
    public void drawUI(Graphics g){
        player.drawScore(g);
        player.drawInv(g);
        player.drawHealth(g);
    }

    public void drawLevel(Graphics g){
        this.level.draw(g);

        //UPDATES            
        //this.level.update(g);
        player.update();
    }

    @Override
    public void update(Graphics g){
        //Nothing here at the moment.
    }
    
    private void isLevelComplete(Player player){
        if(player.onDoor){
            currentLevel += 1;
            player.onDoor = false;
            System.out.println(currentLevel);
        }
    }
}