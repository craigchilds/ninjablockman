package ninjablockman;

import java.awt.Color;
import java.awt.Graphics;
import java.util.concurrent.CopyOnWriteArrayList;

public class Enemy extends Properties{

    String type;
    Boolean destroyed = false, shooting, facingRight;
    static CopyOnWriteArrayList<Projectile> bullets = new CopyOnWriteArrayList<Projectile>();
    
    public Enemy(int health, int damage, int score, float speed, int xPos, int yPos, int height, int width, String type){
        // set all of the enemy properties
        this.health = health;
        this.damage = damage;
        this.score = score;
        this.speed = speed;
        this.xPos = xPos;
        this.yPos = yPos;
        this.height = height;
        this.width = width;
        this.type = type;
    }

    public void draw(Graphics g){
        if(!destroyed){
            g.setColor(new Color(100,100,100));
            g.fillRect(xPos, yPos, width, height);
        }
        
    }

    private void fire(){
        for(int i = 0; i < 1000; i++){
            if(i == 1000){
                i = 0;
                shoot();
            }
        }
    }

    public void update(){
        
    }
    
    private void shoot(){
      // Projectile bullet  = new Projectile(this.xPos, this.yPos + (this.height / 3), this);
       this.shooting = true;
       //bullets.add(bullet);
    }

    
}
