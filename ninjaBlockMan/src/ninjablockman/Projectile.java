package ninjablockman;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Projectile {
    int damage = 10, x, y, height = 5, width = 10, originalX, originalY, range = 350;
    boolean goingRight = false, destroyed= false;
    Image explosion, bullet;
    Graphics g;
    
    public Projectile(int x, int y, Player player){
        this.x = x;
        this.y = y;
        
        this.originalX = x;
        this.originalY = y;

        this.goingRight = player.facingRight;
    }

    public void draw(Graphics g){
        this.g = g;
        if(!this.destroyed){
            g.setColor(new Color(0,0,0));
            g.fillRect(x, y, width, height);
            //this.setBullet("images/bullet.png");
            //g.drawImage(bullet, x, y, null);
        }
    }
    
    public void update(){
        if(!this.destroyed){
            if(this.goingRight){
                this.x += 2;
                if(this.x > this.originalX + this.range){
                    this.destroyed = true;
                    this.setExplosion("images/explosion-small.png");
                    this.g.drawImage(explosion, this.x, this.y, null); 
                }
            }else{
                if(this.x < this.originalX - this.range){
                    this.destroyed = true;
                    this.setExplosion("images/explosion-small.png");
                    this.g.drawImage(explosion, this.x, this.y, null); 
                }
                this.x -= 2;
            }
        }
    }
    
    public void setExplosion(String value){
       try {
           this.explosion = ImageIO.read(new File(value));
       }catch(IOException e){
           System.out.println(e);
       }
    }

    public void setBullet(String value){
       try {
           this.bullet = ImageIO.read(new File(value));
       }catch(IOException e){
           System.out.println(e);
       }
    }

}