package ninjablockman;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Object {
    Boundary bound = new Boundary();
    Rectangle rect = new Rectangle();
    Rectangle[] RectangleArray;
    
    public int value = 10;
    public int x, y, id, height, width;
    public boolean isHit = false, doorOpened = false;
    public String type;

    //Score constructor
    public Object(int x, int y, int width, int height, String type){
        this.x = x * 30;
        this.y = (y * 30) - 20;
        this.width = width;
        this.height = height;
        this.type = type;

        //Set the rectangle properties to that of what we pass.
        rect.height = this.height;
        rect.width = this.width;
        rect.y = this.y;
        rect.x = this.x;

        //Create the rectangle array
        RectangleArray = new Rectangle[1];
        RectangleArray[0] = rect;

    }

    //Add score to the player.
    public void addScore(Player player){
        player.score += value;
        //System.out.println(player.score);
    }

    //Has the player hit a score orb
    public void playerPickup(Player player){
        //If it has not been hit check for collision.
        if(!isHit){
            bound.checkCollision(player.Rect, RectangleArray);
            if(bound.collision[0]){
                isHit = true;
                if(this.type.equals("Score")){
                    addScore(player);
                }
            }
        }else if(this.type.equals("Door")){
            bound.checkCollision(player.Rect, RectangleArray);
            if(bound.collision[0]){
                isHit = true;
                if(this.type.equals("Door")){
                    if(player.hasKey()){
                        //Then use this to proceed to level
                        player.onDoor = true;
                        System.out.println("Player proceeds to next level.");
                    }
                }
            }
        }
    }
    

    //Draw the orb if the player has not hit it.
    public void draw(Graphics g){
        //Only draw if it has not been hit.
        if(!isHit){
            if(this.type.equals("Score")){
                g.setColor(Color.YELLOW);
                g.fill3DRect(this.x, this.y, this.width, this.height, true);
            }
        }
        
        if(this.type.equals("Door")){
             g.setColor(new Color(100,100,100));
             g.fill3DRect(this.x, this.y, this.width, this.height, true);
        }
    }
    
    public void update(Graphics g, Player player){
        playerPickup(player);        
    }
}