package ninjablockman;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Background {
    
    Image building, cloud, grass, grassBlock;
    
    public void draw(Graphics g){
        drawBack(g);
    }
    
    private void drawBack(Graphics g){
        //DRAW SOME BACKGROUND SCENERY
        
        //BUILDINGS
       try {
           building = ImageIO.read(new File("images/building1.png"));
       }catch(IOException e){
           System.out.println(e);
       }
       
       //this.drawCloud(g);
       //g.drawImage(building, 380, 400 - building.getHeight(null), null);
       this.drawFloor(g);
       
     
        
        //TREE
        //g.setColor(Color.green);

       //g.fillRect(420, 150, 40, 40);
    }
    
    private void drawCloud(Graphics g){
       try {
           cloud = ImageIO.read(new File("images/cloud.png"));
       }catch(IOException e){
           System.out.println(e);
       }
       
       g.drawImage(cloud, 120, 10, null);
       g.drawImage(cloud, 50, 40, null);
       
       g.drawImage(cloud, 320, 50, null);
       g.drawImage(cloud, 250, 20, null);
    }
     
    private void drawFloor(Graphics g){
       try {
           grass = ImageIO.read(new File("images/grass.png"));
           grassBlock = ImageIO.read(new File("images/grass-block.png"));
       }catch(IOException e){
           System.out.println(e);
       }
       
       
       
       for(int i = 0; i < 800; i += 10){
           g.drawImage(grass, i, 390, null);
       }
       /*
       for(int i = 0; i < 800; i += 10){
           g.drawImage(grassBlock, i, 390, null);
       }*/
    }  
}
