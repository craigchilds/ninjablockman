package ninjablockman;

import java.awt.Rectangle;

/**
 * @desc The class that checks if a movement can be made or if something has collided with something else.
 * @author Craig
 */
public class Boundary {
    //Rectangle Rect;
    public boolean[] collision;

    //Is the player within the boundaries.
    boolean inBoundary(Player player){
        if(player.yPos < 540 || player.yPos > 30 || player.xPos > 60 || player.xPos < 400 ){
            return true;
        }else{
            return false;
        }
    }

    //Can the player move left?
    public boolean canMoveLeft(Player player){
        if(player.xPos >= 30){
            return true;
        }else{
            return false;
        }
    }

    //Can the player move right?
    public boolean canMoveRight(Player player){
        if(player.xPos <= 540){
            return true;
        }else{
            return false;
        }
    }

    //Tailored collision detection PLAYER - RECTANGLE[]
    //Rectangle represents the rectangle surrounding an object but in an array
    public void checkCollision(Rectangle playerRect, Rectangle[] rect){
        collision = new boolean[rect.length];
        
        for(int i = 0; i < rect.length; i++){
            
            collision[i] = collide(playerRect, rect[i]);
            //debugging
            if(collision[i]){
                //System.out.println(collision[i]);
            }
        }
        
    }
    
    private boolean collide(Rectangle rect1, Rectangle rect2){
        return rect1.intersects(rect2);
    }

    //Take down the players health when they hit a trap.
    public void hit(Player player, Platform trap){
        player.health -= trap.damage;
    }
               
}