package ninjablockman;
//import java.awt.Image;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.imageio.ImageIO;
/**
 *
 * @author Craig Childs
 * @version 0.5.5
 * @date 24/02/2012 15:29
 */
public class Player extends Properties {

   boolean alive = true, facingRight = false;
   int floor = 400, sprintTimer = 1;
   Gravity grav = new Gravity();
   Boundary bound = new Boundary();
   Rectangle Rect = new Rectangle();
   Rectangle feet = new Rectangle();
   Platform[] PlatformRect, LadderRect;
        
   Item item;
   String invtxt;
   String[] inventory;
   boolean onDoor, shooting = false, holdingKey = false, sprinting = false;
   
   static CopyOnWriteArrayList<Projectile> bullets = new CopyOnWriteArrayList<Projectile>();

   //Constructor
   public Player(int health, int damage, int score, float speed, int xPos, int yPos, int height, int width){
       //position
       this.xPos = xPos;
       this.yPos = yPos;
       
       //stats
       this.health = health;
       this.damage = damage;
       this.score = score;
       this.speed = speed;   
       
       //properties
       this.height = height;
       this.width = width;
       
       inventory = new String[3];
       
       System.out.println(health);
  }

   //Make the player aware of the platforms on the level.
   public void addPlatformAwareness(Platform[] platform, Platform[] ladder){
       this.PlatformRect = platform;
       this.LadderRect = ladder;
   }

   public void clearAwareness(){
       for(int i = 0; i < PlatformRect.length; i++){
            PlatformRect[i] = null;
       }
   }
   
   //Add the item to the players inventory
   public void addToInv(Item item){
       if(item.type.equals("KeyCard")){
           inventory[0] = item.type;
           this.holdingKey = true;
       }else if(item.type.equals("Gun")){
           inventory[1] = item.type;
       }else if(item.type.equals("Med Pack")){
           inventory[2] = item.type;
       }
       
       for(int i = 0; i < inventory.length; i++){
           System.out.println(inventory[i]);
       }
       
       this.item = item;
   }
   
   public void use(int id){
       item.doFunction(inventory[id], this);
       if(!inventory[id].equals("Gun") && !inventory[id].equals("KeyCard")){
           inventory[id] = null;
       }
   }
   
   public void drawInv(Graphics g){
       for(int i = 0; i < inventory.length; i++){
           if(inventory[i] == null){
               inventory[i] = "Empty";
           }
       }
       
       g.setColor(Color.YELLOW);
       g.drawString("Inventory:", 720, 40);
       g.drawString(inventory[0], 720, 55);
       g.drawString(inventory[1] + " [W]", 720, 70);
       g.drawString(inventory[2] + " [E]", 720, 85);
   }
   
   public void playerMove_Left(boolean value){
       //Player moves left
       if(value == true){
           this.xPos -= this.speed;
           this.facingRight = false;
       }
   }
   
   public void playerMove_Right(boolean value){
       //Player moves right
      if(value == true){
          this.xPos += this.speed;
          this.facingRight = true;
      }
   }
   
   public void playerJump(boolean value){
       //Player jumps
       this.jumping = value;
       if(this.jumping == true && this.onSomething() && this.hasJumped == false){
           jump();
           this.hasJumped = true;
       }
   }
   
   public void playerDuck(boolean value){
       //Player ducks
       this.ducking = value;
       if(this.ducking == true){
           //duck();
           this.ducking = false;
           //Animation based method, relies on decreased height of the player.
       }
   }

   /**
    * @desc - Increment the players yPos until it reaches the jump height.
    */
   private void jump(){
      this.yPos -= this.jumpRate;
   }

   //Draw the score to the screen.
   public void drawScore(Graphics g){
       g.setColor(Color.YELLOW);
       g.drawString(Integer.toString(score) + " POINTS", 520, 40);
   }
   
   public void drawHealth(Graphics g){
       g.setColor(Color.YELLOW);
       g.drawString(Integer.toString(health) + "%", 20, 40);
   }

   public void draw(Graphics g){
       //Only want to draw the player standing if they are alive.
       if(this.alive){
           //this.image = getImage(image);
           g.setColor(Color.RED);
           //g.fill3DRect(this.xPos, this.yPos, this.width, this.height, true);
           
           if(facingRight){
             this.setImage("images/player-right.png");
           }else{
             this.setImage("images/player-left.png");
           }
           g.drawImage(img, xPos, yPos, null);                      
       }else{
           //else make them lie on the ground by swapping height and width.
           g.setColor(Color.RED);
           //g.fill3DRect(this.xPos, this.yPos + (this.height - 10), this.height, this.width, true);
           g.drawImage(img, xPos, yPos, null);
       }
       
       if(this.shooting){
           for (Projectile bullet : bullets) {
		bullet.draw(g);
		bullet.update();
           }
       }
   }
      
   public void update(){
       //set up the Collision boundary
       this.Rect.height = this.height;
       this.Rect.width = this.width;
       this.Rect.x = this.xPos;
       this.Rect.y = this.yPos;
       
       //Set the values for the players
       this.feet.y = this.yPos + (this.height - 1);
       this.feet.x = this.xPos;
       this.feet.width = this.width;
       this.feet.height = 1;
       
       
       //Debugging collision.
       // System.out.println("PLATFORM TOP:" + (this.PlatformRect[2].Top.y - 30));
       //System.out.println("PLAYER FEET:" + this.feet.y);

       //Check if the player is on a platform.
       grav.platformCheck(this, this.PlatformRect);
       grav.ladderCheck(this, this.LadderRect);
       
       //If the player is on a platform remove gravity so that they stop on the platform.
       if(grav.onPlatform || grav.onLadder){
           grav.remove(this);
       }else{
           //Else set gravity
           grav.apply(this);
       }
       
       
       //Check if they are walking left
       if(this.walking_left){
           //Check if they can move left?
           if(bound.canMoveLeft(this)){
               //move left
               this.playerMove_Left(true);
           }
       }

       //Same system as walking left
       if(this.walking_right){
           if(bound.canMoveRight(this)){
               this.playerMove_Right(true);
           }
       }

       //Same system as walking left
       if(this.jumping){
           if(bound.inBoundary(this)){
               this.playerJump(true);              
           }
       }
       
       //Same system as walking left
       if(this.ducking){
           if(bound.inBoundary(this)){
               this.playerDuck(true);
           }
       }       
   }
   
   //http://docs.oracle.com/javase/tutorial/2d/images/loadimage.html
   public void setImage(String value){
       try {
           this.img = ImageIO.read(new File(value));
       }catch(IOException e){
           System.out.println(e);
       }
   }
   
   public void shoot(){
       Projectile bullet  = new Projectile(this.xPos, this.yPos + (this.height / 3), this);
       this.shooting = true;
       bullets.add(bullet);
   }
   
   public boolean hasKey(){
       return this.holdingKey;
   }

   public void sprint(){
      if(this.onSomething()){
          this.speed = 2.0f;
          this.sprinting = true;
      }       
   }

   public void sprintStop(){
       this.speed = 1.0f;
       this.sprinting = false;
   }

   public boolean onSomething(){
       if(this.yPos + 32 == this.floor || grav.onPlatform || grav.onLadder){
           return true;
       }
       
       System.out.println(this.floor - this.yPos);
       
       return false;
   }
   
}