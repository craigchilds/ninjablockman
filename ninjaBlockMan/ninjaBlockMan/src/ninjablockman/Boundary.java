package ninjablockman;

import java.awt.Rectangle;

/**
 * @desc The class that checks if a movement can be made or if something has collided with something else.
 * @author Craig
 */
public class Boundary {
    //Rectangle Rect;
    public boolean[] collision;

    //Is the player within the boundaries.
    boolean inBoundary(Player player){
        if(player.yPos < 400 || player.yPos > 30 || player.xPos > 10 || player.xPos < 790 ){
            return true;
        }else{
            return false;
        }
    }

    //Can the player move left?
    public boolean canMoveLeft(Player player){
        if(player.xPos >= 60){
            return true;
        }else{
            return false;
        }
    }

    //Can the player move right?
    public boolean canMoveRight(Player player){
        if(player.xPos <= 730){
            return true;
        }else{
            return false;
        }
    }

    //Tailored collision detection PLAYER - RECTANGLE[]
    //Rectangle represents the rectangle surrounding an object but in an array
    public void checkCollision(Player player, Rectangle[] rect){
        collision = new boolean[rect.length];
        
        for(int i = 0; i < rect.length; i++){
            
            collision[i] = collide(player, rect[i]);
            //debugging
            if(collision[i]){
                //System.out.println(collision[i]);
            }
        }
        
    }
    
    private boolean collide(Player player, Rectangle rect){
        return player.Rect.intersects(rect);
    }

    //Take down the players health when they hit a trap.
    public void hit(Player player, Platform trap){
        player.health -= trap.damage;
    }
               
}