package ninjablockman;

import java.awt.Image;
import java.awt.image.BufferedImage;

/**
 *
 * @author Craig Childs
 * @version 0.4.8
 * @date 23/02/2012 11:44
 */
public class Properties {
   //Player stats
   protected int health = 100;
   protected int damage = 10;
   protected int score;
   protected float speed;
   
   //Character position
   protected int xPos;
   protected int yPos;
   protected boolean isOnPlatform = false;
   
   //Graphics
   protected BufferedImage img = null;
   protected int width;
   protected int height;
    
    
   //Movement variables for the character
   protected boolean walking_left = false;
   protected boolean walking_right = false;
   protected boolean jumping = false;
   protected boolean ducking = false;
   protected boolean hasJumped = true;

   //Physics
   protected int jumpRate = 1;
   protected int jumpHeight = 20;

   /**
    * THESE ARE THE SETTERS AND GETTERS FOR THE Character PROPERTIES
    * These do not have to be used but can be to get and set values.
    */
   public void setHealth(int value){
       health = value;
   }

   public int getHealth(){
       return health;
   }

   public void setDamage(int value){
       damage = value;
   }

   public int getDamage(){
       return damage;
   }

   public void setScore(int value){
       score = value;
   }

   public int getScore(){
       return score;
   }

   public void setSpeed(float value){
       speed = value;
   }

   public float getSpeed(){
       return speed;
   }

   public void setXPos(int value){
       xPos = value;
   }

   public int getXPos(){
       return xPos;
   }

   public void setYPos(int value){
       yPos = value;
   }
   
   public void setHasJumped(boolean value){
       hasJumped = value;
   }
   
   protected boolean setLeft(Boolean value){
       return walking_left = value;
   }
   protected boolean setRight(Boolean value){
       return walking_right = value;
   }
   protected boolean setJump(Boolean value){
       return jumping = value;
   }
   protected boolean setDuck(Boolean value){
       return ducking = value;
   }
}