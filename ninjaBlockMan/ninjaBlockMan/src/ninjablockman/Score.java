package ninjablockman;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Score {
    Boundary bound = new Boundary();
    Rectangle rect = new Rectangle();
    Rectangle[] RectangleArray;
    
    public int value = 10;
    public int x, y, id, height, width;
    public boolean isHit = false;

    //Score constructor
    public Score(int x, int y, int width, int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        //Set the rectangle properties to that of what we pass.
        rect.height = this.height;
        rect.width = this.width;
        rect.y = this.y;
        rect.x = this.x;

        //Create the rectangle array
        RectangleArray = new Rectangle[1];
        RectangleArray[0] = rect;

    }

    //Add score to the player.
    public void addScore(Player player){
        player.score += value;
        //System.out.println(player.score);
    }

    //Has the player hit a score orb
    public void playerPickup(Player player){
        if(!isHit){
            bound.checkCollision(player, RectangleArray);
            if(bound.collision[0]){
                isHit = true;
                addScore(player);
            }
        }
    }

    //Draw the orb if the player has not hit it.
    public void draw(Graphics g){
        if(!isHit){
            g.setColor(Color.YELLOW);
            g.fill3DRect(this.x, this.y, this.width, this.height, true);
        }
    }
    
    public void update(Graphics g, Player player){
        playerPickup(player);        
    }
}