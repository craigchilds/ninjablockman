package ninjablockman;
//import java.awt.Image;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
/**
 *
 * @author Craig Childs
 * @version 0.5.5
 * @date 24/02/2012 15:29
 */
public class Player extends Properties {

   boolean alive = true, facingRight = false;
   int floor = 400;
   Gravity grav = new Gravity();
   Boundary bound = new Boundary();
   Rectangle Rect = new Rectangle();
   Rectangle feet = new Rectangle();
   Rectangle[] PlatformRect;
   
   Item item;
   String invtxt;
   String[] inventory;
   

   //Constructor
   public Player(int health, int damage, int score, float speed, int xPos, int yPos, int height, int width){
       //position
       this.xPos = xPos;
       this.yPos = yPos;
       
       //stats
       this.health = health;
       this.damage = damage;
       this.score = score;
       this.speed = speed;   
       
       //properties
       this.height = height;
       this.width = width;
       
       inventory = new String[3];
       
       System.out.println(health);
  }

   //Make the player aware of the platforms on the level.
   public void addPlatformAwareness(Platform[] platform){
       PlatformRect = new Rectangle[platform.length];
       
       for(int i = 0; i < platform.length; i++){
            PlatformRect[i] = platform[i].Rect;
            System.out.println(PlatformRect[i]);
       }
       
   }
   
   //Add the item to the players inventory
   public void addToInv(Item item){
       if(item.type.equals("KeyCard")){
           inventory[0] = item.type;
       }else if(item.type.equals("Gun")){
           inventory[1] = item.type;
       }else if(item.type.equals("Med Pack")){
           inventory[2] = item.type;
       }
       
       for(int i = 0; i < inventory.length; i++){
           System.out.println(inventory[i]);
       }
       
       this.item = item;
   }
   
   public void use(int id){
       item.doFunction(inventory[id], this);
       if(!inventory[id].equals("Gun")){
           inventory[id] = null;
       }
   }
   
   public void drawInv(Graphics g){
       for(int i = 0; i < inventory.length; i++){
           if(inventory[i] == null){
               inventory[i] = "Empty";
           }
       }
       
       g.setColor(Color.YELLOW);
       g.drawString("Inventory:", 720, 40);
       g.drawString(inventory[0] + " [Q]", 720, 55);
       g.drawString(inventory[1] + " [W]", 720, 70);
       g.drawString(inventory[2] + " [E]", 720, 85);
   }
   
   public void playerMove_Left(boolean value){
       //Player moves left
       if(value == true){
           this.xPos -= this.speed;
           this.walking_left = false;
           this.facingRight = false;
       }
   }
   
   public void playerMove_Right(boolean value){
       //Player moves right
      if(value == true){
          this.xPos += this.speed;
          this.walking_right = false;
          this.facingRight = true;
      }
   }
   
   public void playerJump(boolean value){
       //Player jumps
       this.jumping = value;
       if(this.jumping == true && this.hasJumped == false){
           jump();
           this.jumping = false;
           this.hasJumped = true;
       }
   }
   
   public void playerDuck(boolean value){
       //Player ducks
       this.ducking = value;
       if(this.ducking == true){
           //duck();
           this.ducking = false;
           //Animation based method, relies on decreased height of the player.
       }
   }

   /**
    * @desc - Increment the players yPos until it reaches the jump height.
    */
   private void jump(){
      for(int i = 0; i < this.jumpHeight; i++){
       this.yPos -= this.jumpRate * 2;
      }
   }

   //Draw the score to the screen.
   public void drawScore(Graphics g){
       g.setColor(Color.YELLOW);
       g.drawString("Score: " + Integer.toString(score), 20, 40);
   }
   
   public void drawHealth(Graphics g){
       g.setColor(Color.YELLOW);
       g.drawString("Health: " + Integer.toString(health), 20, 55);
   }

   public void draw(Graphics g){
       //Only want to draw the player standing if they are alive.
       if(this.alive){
           //this.image = getImage(image);
           g.setColor(Color.RED);
           //g.fill3DRect(this.xPos, this.yPos, this.width, this.height, true);
           g.drawImage(img, xPos, yPos, null);
       }else{
           //else make them lie on the ground by swapping height and width.
           g.setColor(Color.RED);
           //g.fill3DRect(this.xPos, this.yPos + (this.height - 10), this.height, this.width, true);
           g.drawImage(img, xPos, yPos, null);
       }
   }
      
   public void update(){
       //set up the Collision boundary
       this.Rect.height = this.height;
       this.Rect.width = this.width;
       this.Rect.x = this.xPos;
       this.Rect.y = this.yPos;
       
       //Set the values for the players
       this.feet.y = this.yPos + (this.height - 1);
       this.feet.x = this.xPos;
       this.feet.width = this.width;
       this.feet.height = 1;
       
       //Debugging collision.
       //System.out.println("PLATFORM:" + PlatformRect.y);
       //System.out.println("PLAYER:" + this.Rect.y);

       //Check if the player is on a platform.
       grav.platformCheck(this, PlatformRect);
       
       //If the player is on a platform remove gravity so that they stop on the platform.
       if(grav.onPlatform == true){
           grav.remove(this);
       }else{
           //Else set gravity
           grav.apply(this);
       }
       
       
       //Check if they are walking left
       if(this.walking_left){
           //Check if they can move left?
           if(bound.canMoveLeft(this)){
               //move left
               this.playerMove_Left(true);
           }
       }

       //Same system as walking left
       if(this.walking_right){
           if(bound.canMoveRight(this)){
               this.playerMove_Right(true);
           }
       }

       //Same system as walking left
       if(this.jumping){
           if(bound.inBoundary(this)){
               this.playerJump(true);              
           }
       }
       
       //Same system as walking left
       if(this.ducking){
           if(bound.inBoundary(this)){
               this.playerDuck(true);
           }
       }
       
       
   }
   
   //http://docs.oracle.com/javase/tutorial/2d/images/loadimage.html
   public void setImage(String value){
       try {
           this.img = ImageIO.read(new File(value));
       }catch(IOException e){
           System.out.println(e);
       }
   }
   
}