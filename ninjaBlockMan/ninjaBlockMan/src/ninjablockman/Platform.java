package ninjablockman;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Platform {
    Rectangle Rect = new Rectangle();
    Rectangle Top = new Rectangle();
    Boundary bound = new Boundary();
    Graphics g;
    int damage = 10;
    
    int x, y, height, width;
    boolean dangerous = false;

    //Create a new platform from the foreground class.
    public void newPlatform(int xPos, int yPos, int Width, int Height){
        Foreground fore = new Foreground("Platform", xPos, yPos,  Width, Height);
        
        this.x = xPos;
        this.y = yPos;
        this.height = Height;
        this.width = Width;
        
        this.Rect = fore.getPlatform();
        this.Top = fore.getTop();
    }

    //Draw the platform
    public void draw(Graphics g){
        g.setColor(new Color(155, 115, 44));
        g.fillRect(this.x, this.y, this.width, this.height);
    }

    //Is this platform dangerous?
    public void setDanger(boolean value){
        this.dangerous = value;
    }
    
}