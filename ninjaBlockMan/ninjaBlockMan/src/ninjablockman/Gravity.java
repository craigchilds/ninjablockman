/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ninjablockman;

import java.awt.Rectangle;

public class Gravity {
    public int gravity = 1;
    public int floor = 160;
    public boolean onFloor = false;
    public boolean onPlatform = false;
    
    Boundary bound = new Boundary();

    Gravity() {
        //The constructor
    }
 
    //apply gravity
    public void apply(Player player){
        gravity = 2;
        //If the player is above the floor, make them fall
        if(player.yPos < floor){
            player.yPos += gravity;
        } 
        
    }
    
    //remove gravity
    public void remove(Player player){
       gravity = 0;
    }

    //Check if the player is on the flood
    public void onFloor(Player player){
        if(player.yPos == this.floor){
            onFloor = true;
        }else{
            onFloor = false;
        }
    }
    
    public void platformCheck(Player player,  Rectangle[] platform){
      //run the check collision method. This will set bound.collision array      
      bound.checkCollision(player, platform);
      
      //loop through the platforms to see if  a collision has occurred.
      for(int i = 0; i < bound.collision.length; i++){
          onPlatform = bound.collision[i];
          //if we are on a platform we want to break out of the loop.
          if(onPlatform){
              break;
          }          
      }
      
      //System.out.println(player.Rect.y + player.Rect.height);
    }
    
    //return the value of onFloor
    public boolean getOnFloor(){
        return onFloor;
    }

    //Set floor value
    public void setFloor(int value){
        floor = value;
    }

}