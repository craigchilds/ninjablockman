package ninjablockman;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.Timer;


/**
 *
 * @author Craig Childs
 * @version 0.4.9
 * @date 24/02/2012 18:06
 */
public class NinjaBlockMan extends JFrame implements KeyListener{
    
    Player player = new Player(
            //Health
            100,
            //Damage
            10,
            //Score
            0,
            //Speed
            10.0f,
            //xPos
            50,
            //yPos
            8,
            //height
            30,
            //width
            10);
    
    
    private Graphics Graphics;
    public Image image;
    Gravity gravity = new Gravity();
    Score score1 = new Score(110, 130, 10, 10);
    Score score2 = new Score(300, 170, 10, 10);
    Score score3 = new Score(650, 170, 10, 10);
    Score score4 = new Score(600, 80, 10, 10);
    Item key = new Item("KeyCard", 255, 45);
    Item medpack = new Item("Med Pack", 200, 45);
    Item gun = new Item("Gun", 280, 45);
    
    public long delay = 30;
    
    public int maxPlatforms = 4; 
    public int currentLevel = 1;
    Platform platform = new Platform();
    Platform platform2 = new Platform();
    Platform platform3 = new Platform();
    Platform platform4 = new Platform();

    Platform trap1 = new Platform();

    Platform[] platformArray;
        
      
    public NinjaBlockMan(){
        //Create our menu window
        initializeWindow();
        gravity.setFloor(200);   
        player.setImage("images/player.png");

        if(currentLevel == 1){
            level1();
        }
                
        //Start a timer process so that we can then time the player.               
        Process timer = new Process();
        timer.Start();  

    }
    
    /**
     * @desc set up our window for us.
     */
    private void initializeWindow(){
        setSize(800, 200);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addKeyListener(this);
        setLocationRelativeTo(null);
        setTitle("Ninja Block Man");
        setVisible(true);
        setBackground(new Color(105, 161, 208));
    }
    
    // I WANT THIS DONE WHICH WILL INITIALIZE THE PROGRAM
       
    @Override
    public void paint(Graphics g){
       image = createImage(getWidth(), getHeight());
       Graphics = image.getGraphics();
       g.fillRect(0, 0, getWidth(), getHeight());
       paintComponent(Graphics);
       g.drawImage(image, 0, 0, null);
       repaint(delay);

       //This should only run repaint() at 50fps.
       //http://stackoverflow.com/questions/2972651/java-repaint-component-every-second
      /* ActionListener taskPerformer = new ActionListener() {
          //@Override
          public void actionPerformed(ActionEvent evt) {
            repaint();
          }
       };

       new Timer((int) delay,taskPerformer).start();*/
    }
        
    public void paintComponent(Graphics g){
        //Draw our components to the floor.
        drawBack(g);
        drawFloor(g);
        drawLevel(g, currentLevel);
        drawUI(g);
    }
     
   
    @Override
    public void keyReleased(KeyEvent key){
        if(key.getKeyCode() == KeyEvent.VK_LEFT){
            // Player moves left
            player.setLeft(false);
        }else if(key.getKeyCode() == KeyEvent.VK_RIGHT){
            // Player moves right
            player.setRight(false);
        }else if(key.getKeyCode() == KeyEvent.VK_UP){
            // Player moves right
            player.setJump(false);
            player.setHasJumped(false);
        }else if(key.getKeyCode() == KeyEvent.VK_DOWN){
            // Player moves right
            player.setDuck(false);
        }
       
    }
    
    @Override
    public void keyPressed(KeyEvent key){
        if(key.getKeyCode() == KeyEvent.VK_LEFT){
            // Player moves left
            player.setLeft(true);
        }else if(key.getKeyCode() == KeyEvent.VK_RIGHT){
            // Player moves right
            player.setRight(true);
        }else if(key.getKeyCode() == KeyEvent.VK_UP){
            // Player moves right
            player.setJump(true);
        }else if(key.getKeyCode() == KeyEvent.VK_DOWN){
            // Player moves right
            player.setDuck(true);
        }else if(key.getKeyCode() == KeyEvent.VK_Q){
            player.use(0);
        }else if(key.getKeyCode() == KeyEvent.VK_W){
            player.use(1);
        }else if(key.getKeyCode() == KeyEvent.VK_E){
            player.use(2);
        }else if(key.getKeyCode() == KeyEvent.VK_R){
            player.use(3);
        }
    }
    
    @Override
    public void keyTyped(KeyEvent key){
        //Don't need to do anything here.
    }
    
    /**
     * @DESC Create the level, and all of its components.
     */
    public final void level1(){
        //Start bringing out platforms into play.
        platform.newPlatform(540, 110, 10 ,60);
        platform2.newPlatform(100, 180, 30 ,10);
        platform3.newPlatform(130, 60, 400 ,20);
        platform4.newPlatform(530, 100, 150 ,20);

        //Set out traps
        trap1.newPlatform(150, 160, 30, 5);
        trap1.setDanger(true);
        
        //add them to an array
        platformArray = new Platform[maxPlatforms];
        platformArray[0] = platform;
        platformArray[1] = platform2;
        platformArray[2] = platform3;
        platformArray[3] = platform4;
        
        //pass them to the player so that he knows that they are there, e.g. collision detection
        player.addPlatformAwareness(platformArray);     
        
    }
    
    public void drawUI(Graphics g){
        player.drawScore(g);
        player.drawInv(g);
        player.drawHealth(g);
    }

    public void drawLevel(Graphics g, int id){

        if(id == 1){
            //Here we draw the first level
            platform.draw(g);
            player.draw(g);
            platform2.draw(g);            
            platform3.draw(g);
            platform4.draw(g);
            //trap1.draw(g);
            
            //SCORE
            score1.draw(g);
            score2.draw(g);
            score3.draw(g);
            score4.draw(g);
            
            //ITEMS
            key.draw(g);
            key.update(g, player);
            medpack.draw(g);
            medpack.update(g, player);
            gun.draw(g);
            gun.update(g, player);
            
            //UPDATES            
            score1.update(g, player);
            score2.update(g, player);
            score3.update(g, player);
            score4.update(g, player);
            player.update();
       }
    }
    
    public void drawFloor(Graphics g){
       g.setColor(new Color(4, 137, 29));
       g.fillRect(0, 190, 800, 10); 
    }
    
    public void drawBack(Graphics g){
        //DRAW SOME BACKGROUND SCENERY
        
        //BUILDINGS
        g.setColor(new Color(208,209,210));
        g.fill3DRect(30, 70, 120, 160, true);
        g.fill3DRect(150, 40, 120, 160, true);
        g.fill3DRect(400, 90, 120, 160, true);
        g.fill3DRect(290, 50, 120, 160, true);
        
        
        //TREE
        //g.setColor(Color.green);
        //g.fillRect(420, 150, 40, 40);
    }
}